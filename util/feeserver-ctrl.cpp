
/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2004
** This file has been written by Matthias Richter,
** Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#include <iostream>
#include <vector>
//#include <stdlib.h>
//#include <string.h>
#include <errno.h>
//#include <signal.h>
#include "mrshellprim.h"
#include "FeeClientLibImp.hpp"
#include "rcu_issue.h"
#include <cstdlib>

using namespace std;
using namespace dcs::fee;

void printHelp()
{
  //  fprintf(stderr,"usage: feeserver-ctrl --server <name> "
  //          "[-if <filename>] [-of <filename>] [-c 0x<command>] "
  //          "[-p 0x<parameter>] [-cec <ControlEngine command>] "
  //          "[-v <verbosity>]\n");
  fprintf(stderr,"usage: feeserver-ctrl --server <name> [-if <filename>] "
	  "[-of <filename>] [-cec <ControlEngine command>] "
	  "[-v <verbosity>]\n");
  fprintf(stderr,"-if ,--inputfile FILE  : read from FILE\n");
  fprintf(stderr,"-of ,--outputfile FILE : write to FILE\n");
  fprintf(stderr,"--server               : name of the FeeServer\n");
  //   fprintf(stderr,"-c ,--command          : 16 bit command word\n");
  //   fprintf(stderr,"-p,--parameter         : 16 bit parameter word\n");
  fprintf(stderr,"-cec,--ce-command      : ControlEngine command\n");
  fprintf(stderr,"                         try -cec help for more info\n");
  fprintf(stderr,"-v,--verbosity         : set verbosity level\n");
  fprintf(stderr,"-h,--help              : print this help\n");
  fprintf(stderr,"-i,--info              : print info\n");
  fprintf(stderr,"--version              : print version\n");
}

void printInfo()
{
  printf("\n**********************************************************************\n");
  printf("feeserver-ctrl: command line interface to the FeeServer command channel\n");
  printf("version %s (compiled "__DATE__", "__TIME__")\n\n", VERSION);
  printf("Matthias Richter, University of Bergen\n");
  printf("for full specification of commands check the wiki on\n");
  printf("www.ift.uib.no/~kjeks/wiki\n");
  printf("please send suggestions and bug reports to %s\n\n", PACKAGE_BUGREPORT);
  printf("feeserver-ctrl \t\t\t(%s) \t\t\t%s\n", PACKAGE_TARNAME, VERSION);
}

void printVersion()
{
  printf("%s\n", VERSION);
}

void printCEhelp()
{
  printf("available ControlEngine commands:\n");
  printf("\t\tFEESRV_EXECUTE_PGM\n");
  printf("\t\tFEESRV_EXECUTE_SCRIPT\n");
  printf("\t\tFEESRV_BINARY_PGM\n");
  printf("\t\tFEESRV_RCUSH_SCRIPT\n");
  printf("\t\tCEDBG_SET_BUF_PRINT_SIZE\n");
  printf("\t\tCEDBG_USE_SINGLE_WRITE\n");
  printf("\t\tCEDBG_EN_SERVICE_UPDATE\n");
  printf("\t\tCE_READ_DEFAULT_SERVICES\n");
  printf("\t\tCE_READ_VALID_FECS\n");
  printf("\t\tCE_RELAX_CMD_VERS_CHECK\n");
  printf("\t\tCE_SET_LOGGING_LEVEL\n");
}

/**
 * send a command block to a FeeServer and write result to std output or a file stream
 * The command buffer is transferred to the FeeServer command channel as it is.
 * The target FeeServer must be specified by its name and a FeeClient has to be stated 
 * for the server. NB: one FeeClient can handle more than one FeeServer. 
 *
 * @param pFeeClient  instance of the FeeClient
 * @param pServerName name of the FeeServer to send the command to
 * @param pCmdBuffer  buffer with the command black
 * @param foutput     open file pointer to receive the output stream
 */
int SendFeeServerCmd(FeeClientLibImp* pFeeClient, char* pServerName, char* pCmdBuffer, int iSize, FILE* foutput)
{
  int iResult          = 0;
  if (pFeeClient && pServerName && pCmdBuffer && iSize>0) {
    /* those variables are used to pass arguments to and fetch results from the
     * specific function of the client
     */
    unsigned short            flags     = 0;
    short                     errorCode = 0;
    short                     status    = 0;
    size_t                    size      = iSize;
    std::string               serverName(pServerName);
    std::vector<unsigned char> buffer(size);
    //memcpy(&buffer[0], pCmdBuffer, size);
    std::copy(&(pCmdBuffer[0]), &(pCmdBuffer[iSize]), buffer.begin());

    iResult=pFeeClient->writeReadData(serverName, size, buffer, flags, 
				      errorCode, status);
    if (iResult) {
      cerr << "info: command result size " << size << endl;
      if (size > 0) {
	FILE* fp = stdout;
	if (foutput) fp = foutput;
	fwrite(&(buffer[0]), 1, size, fp);
      }
    } else {
      cerr << "error:  readWriteData iResult = " << iResult << endl;;
      iResult=-EIO;
    }
  } else {
    iResult=-EINVAL;
  }
  return iResult;
}

/* the input buffer descriptor
 */
struct InputBuffer 
{
  int            iSize;
  unsigned char* pBuffer;
};

/* cleanup an input buffer list
 * @param bufferList  reference to the array
 */
int CleanInputBuffers(vector<InputBuffer> & bufferList)
{
  int iResult=0;
  vector<InputBuffer>::iterator element=bufferList.begin();
  while (element!=bufferList.end()) {
    if ((*element).pBuffer) delete [] (*element).pBuffer;
    bufferList.erase(element);
    element=bufferList.begin();
  }
  return iResult;
}

/* read input from a file or the standard input
 * if no file is specified, buffers of the default size are allocated and the std input
 * read into them. The buffers are placed in the buffer list.
 * The function returns the total size of the input
 * @param pFileName            file name
 * @param iDefaultBufferSize   default size for buffer allocation
 * @param bufferList           reference to an array to receive input buffers
 */
int ReadInput(const char* pFileName, int iDefaultBufferSize, vector<InputBuffer> & bufferList)
{
  int   iResult     = 0;
  FILE* fp          = NULL;
  int   iBufferSize = iDefaultBufferSize;
  if (pFileName) {
//     cerr << __FILE__ << __LINE__ << "opening file " << pFileName << endl;
    fp=fopen(pFileName, "r");
    if (fp) {
      fseek(fp, 0, SEEK_END);
      iBufferSize=ftell(fp);
      rewind(fp);
    } else {
      cerr << __FILE__ << __LINE__ << "error: can not open file " << pFileName << endl;
      iResult=-EBADF;
    }
  } else {
//     cerr << __FILE__ << __LINE__ << "read from stdin" << endl;
  }
  if (iResult>=0) {
    if (iBufferSize>0) {
      FILE* finput=fp;
      if (finput==NULL) finput=stdin;
      if (iBufferSize==0) iBufferSize=1024;
      int iSize=0;
      do {
	InputBuffer desc;
	desc.pBuffer=new unsigned char[iBufferSize];
	if (desc.pBuffer) {
	  desc.iSize=fread(desc.pBuffer, 1, iBufferSize, finput);
	  if (desc.iSize>0) {
	    bufferList.push_back(desc);
	    iResult+=desc.iSize;
	  } else {
	    delete [] desc.pBuffer;
	  }
	} else {
	  cerr << __FILE__ << __LINE__ << "error: no memory available, failed to allocate " << iBufferSize << " bytes" << endl;
	  iResult=-ENOMEM;
	}
	if (ferror(finput)) {
	  iResult=-EIO;
	}
// 	if (iResult>=0)
// 	  cerr << __FILE__ << __LINE__ << "accumulated " << iResult << " bytes" << endl;
// 	else 
// 	  cerr << __FILE__ << __LINE__ << "abort reading with error code " << iResult << endl;
      } while (iResult>0 && !feof(finput));
      if (fp) fclose(fp);
      fp=NULL;
      if (iResult<=0) {
	CleanInputBuffers(bufferList);
      }
    }
  }
  return iResult;
}

/* write data from all the input blocks to one monolythic buffer
 * @param bufferList      vector list of buffer descriptors
 * @param pTgtBuffer      the target buffer
 * @param iTgtSize        size of the target buffer
 * @return number of bytes written to the buffer if succeeded
 *         negative error code in case of error
 */
int MakeMonolythicDataBlock(vector<InputBuffer> & bufferList, unsigned char* pTgtBuffer, int iTgtSize)
{
  int iResult=0;
  vector<InputBuffer>::iterator element=bufferList.begin();
  while (element!=bufferList.end() && iResult>=0 && iResult<iTgtSize) {
    int iCopy=(*element).iSize;
    if (iTgtSize-iResult<iCopy) iCopy=iTgtSize-iResult;
    if ((*element).pBuffer) {
      memcpy(pTgtBuffer+iResult, (*element).pBuffer, iCopy);
	iResult+=iCopy;
    } else {
      cerr << __FILE__ << __LINE__ << "error: invalid input buffer descriptor" << endl;
    }
    element++;
  }
  return iResult;
}

class FeeCommandBuffer {
public:
  FeeCommandBuffer();
  ~FeeCommandBuffer();

  /* initialize the command part of the header and internal variables due to the specific command
   */
  int SetCommand(__u32 command);

  /* initialize the command part of the header and internal variables due to the specific command
   * from the command line argumants
   */
  int SetHeaderFromOptions(TArgDef* pOptions);

  /* set the payload for this command buffer 
   * together with the number of header and tailer words (which depend upon the command) this
   * determines the buffer size. The buffer is allocated and the payload copied to the
   * appropriate place.
   * The function might "steal" the data buffer from the input vector in case of only one input buffer
   * in conjunction with no given commands
   * @param  iPayloadSize   total size of data in the buffer list  
   * @param  bufferList     the input buffer list
   */
  int SetPayload(int iPayloadSize, vector<InputBuffer>& bufferList);

  /* get the command buffer
   */
  int GetCommandBuffer(char** ppBuffer, int* pSize);

  /* check whether the current active command requires data from the input
   */
  int CheckReadInput();

private:
  int BuildHeader(int iBuffer);
  int BuildTailer(int iBuffer);

  /** the number of 32-bit header words before the payload buffer */
  int fNofHeaderWords;
  /** size of the payload */
  int fPayloadSize;
  /** payload buffer is padded to 4, size of it  */
  int fPayloadBufferSize;
  /** the number of 32-bit words after the payload */
  int fNofTailerWords;

  __u32* fpBuffer;
  int fBufferSize;

  // command part (upper 16 bit) of the header
  __u32 fCommand;

  // indicates whether this command needs reading the input
  int fbReadInput; 

  // additionale parameters extracted from command line arguments
  // the meaning of this parameters varies between the commands
  int fIntParam;
  int fAddress;
  const char* fStringParam;
};

FeeCommandBuffer::FeeCommandBuffer()
{
  fNofHeaderWords=0;
  fPayloadSize=0;
  fPayloadBufferSize=0;
  fNofTailerWords=0;
  fpBuffer=NULL;
  fBufferSize=0;
  fCommand=0;
  fbReadInput=1;
  fIntParam=0;
  fAddress=0;
  fStringParam=NULL;
}

FeeCommandBuffer::~FeeCommandBuffer()
{
  if (fpBuffer) {
    delete [] fpBuffer;
  }
}

int FeeCommandBuffer::SetCommand(__u32 command)
{
  int iResult=0;
  if (command>0) {
    if (fCommand!=0 && command!=fCommand) {
      cerr << __FILE__ << __LINE__ << "Warning: overriding prviously specified command" << endl;
    }
    fCommand=command;
    switch (fCommand) {
    case FEESRV_EXECUTE_PGM:
    case FEESRV_EXECUTE_SCRIPT:
    case FEESRV_BINARY_PGM:
      fbReadInput=1;
      fNofHeaderWords=1;
      fNofTailerWords=1;
      break;
    case FEESRV_RCUSH_SCRIPT:
      fbReadInput=1;
      fNofHeaderWords=1;
      if (fStringParam!=NULL && strlen(fStringParam)>0) {
	int iLen=(strlen(fStringParam)-1)/4;
	fNofHeaderWords+=iLen+1;
      }
      fNofTailerWords=1;
      break;
    default:
      // CEDBG_SET_BUF_PRINT_SIZE:
      // CEDBG_USE_SINGLE_WRITE:
      // CEDBG_EN_SERVICE_UPDATE:
      // CE_RELAX_CMD_VERS_CHECK:
      // CE_SET_LOGGING_LEVEL:
      // CE_READ_DEFAULT_SERVICES:
      // CE_READ_VALID_FECS:
      fbReadInput=0;
      fNofHeaderWords=1;
      fNofTailerWords=1;
    }
  }
  return iResult;
}

int FeeCommandBuffer::SetHeaderFromOptions(TArgDef* pOptions)
{
  int iResult=0;
  if (pOptions) {
    int iLocalResult=0;
    __u32 command=0;
    if ((iLocalResult=mrShellPrimGetInt(pOptions, "CEDBG_SET_BUF_PRINT_SIZE", &fIntParam))>=0 && ARGPROC_EXISTS(iLocalResult)) command=CEDBG_SET_BUF_PRINT_SIZE;
    if ((iLocalResult=mrShellPrimGetInt(pOptions, "CEDBG_USE_SINGLE_WRITE"  , &fIntParam))>=0 && ARGPROC_EXISTS(iLocalResult)) command=CEDBG_USE_SINGLE_WRITE;
    if ((iLocalResult=mrShellPrimGetInt(pOptions, "CEDBG_EN_SERVICE_UPDATE" , &fIntParam))>=0 && ARGPROC_EXISTS(iLocalResult)) command=CEDBG_EN_SERVICE_UPDATE;
    if ((iLocalResult=mrShellPrimGetInt(pOptions, "CE_RELAX_CMD_VERS_CHECK" , &fIntParam))>=0 && ARGPROC_EXISTS(iLocalResult)) command=CE_RELAX_CMD_VERS_CHECK;
    if ((iLocalResult=mrShellPrimGetInt(pOptions, "CE_SET_LOGGING_LEVEL"    , &fIntParam))>=0 && ARGPROC_EXISTS(iLocalResult)) command=CE_SET_LOGGING_LEVEL;
    if ((iLocalResult=mrShellPrimGetData(pOptions,"FEESRV_RCUSH_SCRIPT="    , (void**)&fStringParam, eConstString))>=0 && ARGPROC_EXISTS(iLocalResult)) {
      command=FEESRV_RCUSH_SCRIPT;
    }
    if (command>0) iResult=SetCommand(command);
  } else {
    cerr << __FILE__ << __LINE__ << "SetHeaderFromOptions error: invalid parameter" << endl;
    iResult=-EINVAL;
  }
  return iResult;
}

int FeeCommandBuffer::BuildHeader(int offset) 
{
  int iResult=0;
  if (fpBuffer) {
    int i=offset;
    //fprintf(stderr, "i=%d fNofHeaderWords=%d fBufferSize=%d\n", i, fNofHeaderWords, fBufferSize);
    if (fNofHeaderWords>0) {
      if (i+fNofHeaderWords<fBufferSize*4) {
	switch (fCommand) {
	case FEESRV_EXECUTE_PGM:
	case FEESRV_EXECUTE_SCRIPT:
	case FEESRV_BINARY_PGM:
	  fpBuffer[i++]=fCommand;//|(0xffff & fPayloadSize);
	  break;
	case FEESRV_RCUSH_SCRIPT:
	  {
	    int iArgumentSize=0;
	    if (fStringParam) iArgumentSize=fNofHeaderWords-1;
	    fpBuffer[i++]=fCommand|(0xffff & (iArgumentSize*4));
	    if (fStringParam) {
	      strcpy((char*)&fpBuffer[i], fStringParam);
	      i+=iArgumentSize;
	    }
	  }
	  break;
	case CEDBG_SET_BUF_PRINT_SIZE:
	case CEDBG_USE_SINGLE_WRITE:
	case CEDBG_EN_SERVICE_UPDATE:
	case CE_RELAX_CMD_VERS_CHECK:
	case CE_SET_LOGGING_LEVEL:
	  fpBuffer[i++]=fCommand|(0xffff & fIntParam);
	  break;
	default:
	  // CE_READ_DEFAULT_SERVICES:
	  // CE_READ_VALID_FECS:
	  fpBuffer[i++]=fCommand;
	}
	iResult=i;
      } else {
	cerr << __FILE__ << __LINE__ << "error: internal buffer size missmatch" << endl;
	iResult=-EFAULT; 
      }
    }
    if (iResult>fNofHeaderWords) {
      cerr << __FILE__ << __LINE__ << "error: possible memory corruption" << endl;
    }
  } else {
    cerr << __FILE__ << __LINE__ << "error: internal initialization missmatch" << endl;
    iResult=-ENODATA; 
  }
  return iResult;
}

int FeeCommandBuffer::BuildTailer(int offset) 
{
  int iResult=0;
  if (fpBuffer) {
    int i=offset;
    //fprintf(stderr, "BuildTailer: offset=%d fNofTailerWords=%d fBufferSize=%d\n", offset, fNofTailerWords, fBufferSize);
    if (fNofTailerWords>0) {
      if (i+fNofTailerWords<fBufferSize*4) {
	switch (fCommand) {
	  // add specific handling for certain commands
	default:
	  fpBuffer[i++]=CE_CMD_TAILER;
	}
      } else {
	cerr << __FILE__ << __LINE__ << "error: internal buffer size missmatch" << endl;
	iResult=-EFAULT; 
      }
    }
    if (iResult>fBufferSize*4) {
      cerr << __FILE__ << __LINE__ << "error: possible memory corruption" << endl;
    }
  } else {
    cerr << __FILE__ << __LINE__ << "error: internal initialization missmatch" << endl;
    iResult=-ENODATA; 
  }
  return iResult;
}

int FeeCommandBuffer::SetPayload(int iPayloadSize, vector<InputBuffer>& bufferList)
{
  int iResult=0;
  if (fpBuffer==NULL && fPayloadSize==0) {
    if (iPayloadSize>0) {
      fPayloadSize=iPayloadSize;
      fPayloadBufferSize=1+(fPayloadSize-1)/4;
    } else {
      fPayloadSize=0;
      fPayloadBufferSize=0;
    }
    //fprintf(stderr, "SetPayload: %d bytes -> buffer size=%d\n", fPayloadSize, fPayloadBufferSize);
    fBufferSize=fNofHeaderWords+fPayloadBufferSize+fNofTailerWords;
    //fprintf(stderr, "SetPayload: allocate buffer of size %d\n", fBufferSize);
    fpBuffer=new __u32[fBufferSize];
    if (fpBuffer) {
      memset(fpBuffer, 0, sizeof(__u32)*fBufferSize);
      int i=0;
      // finalize header
      if ((iResult=BuildHeader(i))>=0) {
	// copy payload into buffer
	i+=iResult;
	int iCopied=0;
	vector<InputBuffer>::iterator element=bufferList.begin();
	while (element!=bufferList.end() && iResult>=0 && iCopied<fPayloadBufferSize*4) {
	  int iCopy=(*element).iSize;
	  if (fPayloadBufferSize*4<iCopied+iCopy) {
	    iCopy=fPayloadBufferSize*4-iCopied;
	    cerr << __FILE__ << __LINE__ << "warning: buffer too small to receive full payload" << endl;
	  }
	  if ((*element).pBuffer) {
	    memcpy(((char*)&(fpBuffer[i]))+iCopied, (*element).pBuffer, iCopy);
	    iCopied+=iCopy;
	  } else {
	    cerr << __FILE__ << __LINE__ << "error: invalid input buffer descriptor" << endl;
	  }
	  element++;
	}
	if (iResult>=0) {
	  // finalize the tailer
	  i+=fPayloadBufferSize;
	  iResult=BuildTailer(i);
	}
      }
    } else {
      iResult=-ENOMEM;
    }
  } else if (iPayloadSize!=0) {
    cerr << __FILE__ << __LINE__ << "error: payload already set" << endl;
  }
  return iResult;
}

int FeeCommandBuffer::GetCommandBuffer(char** ppBuffer, int* pSize)
{
  int iResult=0;
  if (ppBuffer && pSize) {
    //fprintf(stderr, "GetCommandBuffer: header=%#x size=%d\n", *fpBuffer, fBufferSize*4); 
    *ppBuffer=(char*)fpBuffer;
    *pSize=fBufferSize*4;
  } else {
    iResult=-EINVAL;
  }
  return iResult;
}

int FeeCommandBuffer::CheckReadInput()
{
  return fbReadInput;
}


// this global variable will be set during argument scan automatically for those
// CE command which does not require additional parameters
unsigned int g_cecommand=0;

/* @struct ceCommandArgs
 * @ingroup feeserver-ctrl 
 * @brief Definition of the sub-arguments to the --ce-command argument
 */
TArgDef ceCommandArgs[] = {
  {NULL,"FEESRV_EXECUTE_PGM",      {eFlag,    {(void*)&g_cecommand}},{(void*)FEESRV_EXECUTE_PGM},      0},
  {NULL,"FEESRV_EXECUTE_SCRIPT",   {eFlag,    {(void*)&g_cecommand}},{(void*)FEESRV_EXECUTE_SCRIPT},   0},
  {NULL,"FEESRV_BINARY_PGM",       {eFlag,    {(void*)&g_cecommand}},{(void*)FEESRV_BINARY_PGM},       0},
  {NULL,"FEESRV_RCUSH_SCRIPT=",    {eConstString,{NULL}            },{(void*)NULL},                    ARGDEF_UNTERM_LONG},
  {NULL,"FEESRV_RCUSH_SCRIPT",     {eFlag,    {(void*)&g_cecommand}},{(void*)FEESRV_RCUSH_SCRIPT},     0},
  {NULL,"CEDBG_SET_BUF_PRINT_SIZE",{eInteger, {(void*)NULL        }},{(void*)NULL},                    0},
  {NULL,"CEDBG_USE_SINGLE_WRITE",  {eInteger, {(void*)NULL        }},{(void*)NULL},                    0},
  {NULL,"CEDBG_EN_SERVICE_UPDATE", {eInteger, {(void*)NULL        }},{(void*)NULL},                    0},	
  {NULL,"CE_READ_DEFAULT_SERVICES",{eFlag,    {(void*)&g_cecommand}},{(void*)CE_READ_DEFAULT_SERVICES},0},	
  {NULL,"CE_READ_VALID_FECS",      {eFlag,    {(void*)&g_cecommand}},{(void*)CE_READ_VALID_FECS},      0},
  {NULL,"CE_RELAX_CMD_VERS_CHECK", {eInteger, {(void*)NULL        }},{(void*)NULL},                    0},	
  {NULL,"CE_SET_LOGGING_LEVEL",    {eInteger, {(void*)NULL        }},{(void*)NULL},                    0},
  {"*", "help",                    {eFctNoArg,{(void*)printCEhelp }},{(void*)NULL},ARGDEF_TERMINATE},
  {""  ,""                    ,    {eUnknownType, {NULL}},{NULL},0}
};

/* @struct ceCommandScanMode
 * @ingroup feeserver-ctrl 
 * @brief Scan mode for sub-argument scanning for the --ce-command argument
 */
TFctMode ceCommandScanMode = {
  SCANMODE_READ_ONE_CMD, //unsigned int flags;
  NULL,                  //const char* pSeparators;
  NULL                   //FILE* pOutput;
};

extern unsigned int g_mrShellPrimDbg;

/* @struct mainArgs
 * @ingroup feeserver-ctrl 
 * @brief Definition of the main arguments
 */
TArgDef mainArgs[] = {
  {"-h", "--help",         {eFctNoArg, {(void*)printHelp}},{NULL},ARGDEF_TERMINATE},
  {"-i", "--info",         {eFctNoArg, {(void*)printInfo}},{NULL},ARGDEF_TERMINATE},
  {NULL, "--version",      {eFctNoArg, {(void*)printVersion}},{NULL},ARGDEF_TERMINATE},
  {"-if","--inputfile",    {eConstString, {NULL}},{NULL},0},
  {"-of","--outputfile",   {eConstString, {NULL}},{NULL},0},
  {NULL, "--server",       {eConstString, {NULL}},{NULL},0},
  {"-c", "--command=",     {eHex, {NULL}},{NULL},ARGDEF_UNTERM_LONG},
  {"-p", "--parameter=",   {eHex, {NULL}},{NULL},ARGDEF_UNTERM_LONG},
  {"-f", NULL,   {eFloat, {NULL}},{NULL},0},
  {"-cec","--ce-command",  {eFctRemaining, {(void*)ceCommandArgs}},{(void*)&ceCommandScanMode},0},
  {"-v", "--verbosity",    {eInteger, {NULL}},{NULL},0},
  {"--mrsdbg",NULL,        {eHexArray, {(void*)&g_mrShellPrimDbg}},{(void*)1},0},
  {"--mrshelp",NULL,       {eFctNoArg, {(void*)mrShellPrimPrintDbgFlags}},{(void*)1},ARGDEF_TERMINATE},
  {"",   "",               {eUnknownType, {NULL}},{NULL},0},
};


int main(int argc, char** arg) 
{
  int iResult=0;
  int iArg=1;
  int iVerbosity=1;
  const char* pInputFile=NULL;
  const char* pOutputFile=NULL;
  const char* pServerName=NULL;
  if (getenv("DIM_DNS_NODE")==NULL) {
    cerr << "DIM_DNS_NODE undefined, please set the environment to the location of the DIM Name Server, Exiting!" << endl;
    exit(-1);
  }
  iResult=ScanArguments((const char**)arg+iArg, argc-iArg, 0, mainArgs, NULL);

  if (iResult>=0) {
    // check which arguments have been passed to the program and set corresponding variables 
    if (mrShellPrimGetData(mainArgs, "-if", (void**)&pInputFile, eConstString)<0) pInputFile=NULL;
    if (mrShellPrimGetData(mainArgs, "-of", (void**)&pOutputFile, eConstString)<0) pOutputFile=NULL;
    if (mrShellPrimGetData(mainArgs, "--server", (void**)&pServerName, eConstString)<0) pServerName=NULL;
    mrShellPrimGetInt(mainArgs, "-v", &iVerbosity);
    
    //fprintf(stderr,"iResult=%#x g_cecommand=%#x server %s\n", iResult, g_cecommand, pServerName);
    //exit(0);

    FeeClientLibImp* feeClient = new FeeClientLibImp();
    if (pServerName) {
      if (feeClient) {
	if (feeClient->registerFeeServerName(pServerName)) {
	  FILE* fpOutput=NULL;
	  if (pOutputFile) {
	    fpOutput=fopen(pOutputFile, "w");
	    if (!fpOutput) {
	      cerr << __FILE__ << __LINE__ << "error: can not open file " << pOutputFile << " for writing" << endl;
	    }
	  }
	  //cerr << "info: Starting FeeServer client" << endl;
	  if (feeClient->startFeeClient()) {
	    //cerr << "info: FeeClient started" << endl;
	    //sleep (1);

	    // check for ControlEngine command, build header if available and set it as first data word
	    FeeCommandBuffer cmdBuffer;
	    TArgDef* pCEArgs=NULL;
	    if (g_cecommand>0) iResult=cmdBuffer.SetCommand(g_cecommand);
	    else if ((iResult=mrShellPrimGetData(mainArgs, "--ce-command", (void**)&pCEArgs, eFctRemaining))>=0 && ARGPROC_EXISTS(iResult)) {
	      //fprintf(stderr, "ce command found (%d) index %d\n", ARGPROC_EXISTS(iResult), ARGPROC_INDEX(iResult));
	      iResult=cmdBuffer.SetHeaderFromOptions(pCEArgs);
	    }

	    // read the input stream from either the std input or a file 
	    int iPayloadSize=0;
	    vector<InputBuffer> bufferList;
	    if (cmdBuffer.CheckReadInput()) {
	      iPayloadSize=ReadInput(pInputFile, 1024, bufferList);
	      if (iPayloadSize>=0) {
		//cerr << "info read " << iPayloadSize << " byte(s) in " << bufferList.size() << " buffer(s)" << endl;
	      } else {
		cerr << "error while reading input (" << iResult << ")" << endl;
		iResult=-EIO;
	      }
	    } else {
	      // cerr << __FILE__ << __LINE__ << "skip reading input" << endl;
	    }
	    iResult=cmdBuffer.SetPayload(iPayloadSize, bufferList);

	    char* pCmdBuffer=NULL;
	    int iCmdBufferSize=0;
	    iResult=cmdBuffer.GetCommandBuffer(&pCmdBuffer, &iCmdBufferSize);
	    if (iResult>=0 && pCmdBuffer) {
	      SendFeeServerCmd(feeClient, (char*)pServerName, pCmdBuffer, iCmdBufferSize, fpOutput);
	    } else if (pCmdBuffer==NULL) {
	      cerr << "empty command buffer" << endl;
	    }
	    CleanInputBuffers(bufferList);
	    feeClient->stopFeeClient();
	  } else {
	    cerr << __FILE__ << __LINE__ << "error: start FeeServer client failed" << endl;
	  }
	} else {
	  cerr << __FILE__ << __LINE__ << "error: failed to register FeeServer name" << endl;
	}
	delete feeClient;
	feeClient=NULL;
      } else {
	cerr << __FILE__ << __LINE__ << "error: failed to create FeeClient instance" << endl;
      }
    } else {
      printHelp();
      cerr << "please specifiy the server name" << endl;
    }
  } else if (iResult!=-EINTR) {
    cout << "invalid arguments, try --help" << endl;
  }
  return iResult;
}
