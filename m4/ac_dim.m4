dnl -*- mode: autoconf -*- 
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DIM],
[
  AC_REQUIRE([AC_DIM_ARCH])
  dnl ------------------------------------------------------------------
  AC_ARG_ENABLE([builtin-dim],
	        [AC_HELP_STRING([--enable-builtin-dim],
	                        [Use shipped DIM, not systems])])
  have_dim=no
  AC_LANG_PUSH(C++)
  AC_CHECK_LIB(dim, [dic_get_server],
	       [AC_CHECK_HEADERS([dim/dim.hxx],[have_dim=yes])])
  AC_LANG_POP(C++)
  if test "x$enable_builtin_dim" = "xyes" ; then 
     have_dim=no
  fi
  AC_MSG_CHECKING(whether to use possible system DIM installed)
  if test "x$have_dim" = "xno" ; then 
     AC_CONFIG_SUBDIRS(dim)
  fi
  AC_MSG_RESULT($have_dim)
  AM_CONDITIONAL(NEED_DIM, test "x$have_dim" = "xno")
])
dnl
dnl EOF
dnl
