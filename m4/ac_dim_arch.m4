dnl -*- mode: autoconf -*- 
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DIM_ARCH],
[
  AC_REQUIRE([AC_PROG_CC])
  dnl ------------------------------------------------------------------
  dnl Byte order 
  AH_TEMPLATE(MIPSEB, [Big-endian machine])
  AH_TEMPLATE(MIPSEL, [Little-endian machine])
  AC_C_BIGENDIAN([AC_DEFINE([MIPSEB])],[AC_DEFINE([MIPSEL])])
  AC_DEFINE([PROTOCOL],[1])

  dnl ------------------------------------------------------------------
  dnl Thread flags to use 
  case $host_os:$host_cpu in
  solaris*|sun*)	  CFLAGS="$CFLAGS -mt"
			  LIBS="$LIBS -lposix4"		;;
  hp-ux*|osf*|aix*)					;;
  # The DIM libary should not be threaded on an ARM chip, but the
  # FeeServer  assumes that it is so we take the next line out and
  # default to normal Linux
  # linux*:arm*)					;;
  linux*)		  LIBS="$LIBS -pthread"		;;
  lynxos*:rs6000)	  CFLAGS="$CFLAGS -mthreads"	;;
  *)			  LIBS="$LIBS -lpthread"	;;
  esac

  dnl ------------------------------------------------------------------
  dnl Misc flags per host OS/CPU
  case $host_os:$host_cpu in
  sun*)		  AC_DEFINE([sunos])		;;
  solaris*)	  AC_DEFINE([solaris])	
		  LIBS="$LIBS -lsocket -lnsl"	;;
  hp-ux*)	  AC_DEFINE([hpux])		;;
  osf*)		  AC_DEFINE([osf])		;;
  aix*)		  AC_DEFINE([aix])	
		  AC_DEFINE([unix])
		  AC_DEFINE([_BSD])		;;
  lynxos*:rs6000) AC_DEFINE([LYNXOS])	
		  AC_DEFINE([RAID])
		  AC_DEFINE([unix])		
		  CPPFLAGS="$CPPFLAGS -I/usr/include/bsd -I/usr/include/posix"
		  LDFLAGS="$LDFLAGS -L/usr/posix/usr/lib"
		  LIBS="$LIBS -lbsd"		;;
  lynxos*:*86*)	  AC_DEFINE([LYNXOS])	
		  AC_DEFINE([unix])		
		  LIBS="$LIBS -lbsd -llynx"	;;
  lynxos*)	  AC_DEFINE([LYNXOS])	
		  AC_DEFINE([unix])		
		  LIBS="$LIBS -lbsd"		;;
  linux*)	  AC_DEFINE([linux])		
		  AC_DEFINE([unix])		;;
  esac
])
dnl
dnl EOF
dnl
