// -*- mode: C++ -*-
#ifndef DCS_FEE_FEE_CLIENT_LIB_IMP_H
#define DCS_FEE_FEE_CLIENT_LIB_IMP_H
#ifndef DCS_FEE_FEE_CLIENT_LIB_INTERFACE_HPP
# include <FeeClientLibInterface.hpp>
#endif
#ifndef DCS_FEE_MESSAGE_STRUCT_HPP
# include <MessageStruct.hpp>
#endif
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __STRING__
# include <string>
#endif
#ifndef FEECLIENT_API
#define FEECLIENT_API
#endif

namespace dcs 
{
  namespace fee 
  {
    //====================================================================
    /** @struct FeeClientLibImp
	@brief Dummy client interface 
	@ingroup feexx
    */
    struct FEECLIENT_API FeeClientLibImp : public dcs::fee::FeeClientLibInterface
    {
      /** Constructor */
      FeeClientLibImp() {}
      /** Destructor */
      virtual ~FeeClientLibImp() {}
    
      /** Type of flags */
      typedef unsigned short FlagBits;
      /** Type of data array */
      typedef std::vector<unsigned char> DataArray;

      /** Send a command
	  @param serverName Address of server name string.  On return,
	  contains pointer to newly allocated memory with string of
	  responding server name  
	  @param size Input data size (in elements).  On return the size of
	  the returned data (in elements). 
	  @param data Message data.  On return the possible return data. 
	  @param flags Message flags 
	  @param errorCode On return,  error code if any
	  @param status On return, the status
	  @return @c true (1) on success, @c false (0) otherwise */
      bool writeReadData(std::string&  serverName, 
			 size_t&       size, 
			 DataArray&    data, 
			 FlagBits&     flags, 
			 short&        errorCode, 
			 short&        status);
      /** Get an aknowledgement 
	  @param serverName Name of server responding
	  @param size Size of data returned
	  @param data data returned from server. 
	  @param id Identifier obtained by sendCommand
	  @param flags Flags used 
	  @param errorCode Return error code
	  @param status Return status code 
	  @return time left before timeout, or 0 on timeout */
      int getAcknowledge(std::string&  serverName, 
			 size_t&       size, 
			 DataArray&    data, 
			 unsigned int  id, 
			 FlagBits&     flags, 
			 short&        errorCode, 
			 short&        status);
    protected:
      /** Log messages */
      void FeeLogging(int         type, 
		      int         ref, 
		      int         id, 
		      const char* func, 
		      const char* format, 
		      ...);
      /** Handle acknowledgments
	  @param serverName Server 
	  @param size data size 
	  @param data data
	  @param id Id 
	  @param flags Flags
	  @param errorCode Error code
	  @param status status */
      void acknowledgeHandler(char*        serverName, 
			      unsigned int size, 
			      char*        data, 
			      unsigned int id, 
			      FlagBits     flags, 
			      short        errorCode, 
			      short        status);
      /** Handle messages
	  @param msg Message */
      void messageHandler(const dcs::fee::MessageStruct msg);
    private:
      /** Current server name */
      std::string curServerName;
      /** Current data size (in bytes) */
      size_t curSize;
      /** Current data */
      DataArray curData;
      /** Current ID */
      unsigned int curId;
      /** Current flags */
      FlagBits     curFlags;
      /** Current error code */
      short        curErrorCode;
      /** Current status */
      short        curStatus;
    };
  }
}

#endif
//
// EOF
//
