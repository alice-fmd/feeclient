#include "FeeClientLogger.hpp"
#include "FeeClientLibInterface.hpp"
#include "Timestamp.hpp"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdarg>
#include <cstdio>

using namespace dcs::fee;

FeeClientLogger* FeeClientLogger::pInstance = 0;

FeeClientLogger::~FeeClientLogger() {
  if (pInstance == 0) {
    return;
  }

  // set pInstance to 0
  pInstance = 0;
  mpInterface = 0;
}

bool FeeClientLogger::registerInterface(FeeClientLibInterface* interface) {
  if (mpInterface == 0) {
    mpInterface = interface;
    return true;
  }
  return false;
}

FeeClientLogger* FeeClientLogger::getLogger() {
  if (pInstance == 0) {
    pInstance = new FeeClientLogger();
  }
  return pInstance;
}

void FeeClientLogger::createLogMessage(unsigned int type, char* origin,
				       char* description) const {
  // check LogLevel
  if (!checkLogLevel(type)) {
    return;
  }

  Timestamp now;

  MessageStruct msg(type, DETECTOR, origin, description, now.date);
  relayLogEntry(&msg);
}

void FeeClientLogger::relayLogEntry(MessageStruct* msgStruct) const {
  // check LogLevel
  if (!checkLogLevel(msgStruct->eventType)) {
    return;
  }

  if (mpInterface != 0) {
    // call of the corresponding handler routine of the interface
    mpInterface->messageHandler(*msgStruct);
  }
}

size_t dcs::fee::DebugGuard::mLevel = 0;
bool   dcs::fee::DebugGuard::mDebug = false;

dcs::fee::DebugGuard::DebugGuard(const char* where, const char* format, ...)
  : mWhere(where)
{
  if (!mDebug) return;
  static char buf[1024];
  va_list ap;
  va_start(ap, format);
  vsprintf(buf,format, ap);
  va_end(ap);
  mWhat = buf;
  makeMessage("<=");
  mLevel++;
}
	
dcs::fee::DebugGuard::~DebugGuard()
{
  if (!mDebug) return;
  mLevel--;
  makeMessage("=>");
}
  
void
dcs::fee::DebugGuard::makeMessage(const char* prefix) 
{
  std::cout << std::setw(mLevel+2) << prefix << mWhere << ": " << mWhat
	    << std::endl;
}
