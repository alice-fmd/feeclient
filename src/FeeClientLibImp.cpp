#include <FeeClientLibImp.hpp>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <cstdarg>
#include <sstream>
#include <cstdio>
// #include <pthread.h>
#ifndef WIN32
#include <unistd.h>
#else
#include <windows.h>
#endif

//====================================================================
#define logLevelAll       0
#define logLevelBenchmark 1
#define logLevelDebug     2
#define logLevelInfo      3
#define logLevelWarning   4
#define logLevelError     5
#define logLevelFatal     6
#define logLevelNone      7
#define logLevel          logLevelAll

// Client flags 
#define FEECLIENTFLAGS 0x0 // bit 0x2 to switch on check sum
namespace 
{
  //====================================================================
  struct FeeException : public std::exception
  {
    std::string fWhat;
    virtual ~FeeException() throw () {}
    FeeException(const char* func, const char* format, ...)
      : fWhat("")
    {
      static char buf[1024];
      va_list ap;
      va_start(ap, format);
      vsprintf(buf,format, ap);
      va_end(ap);
      std::stringstream s;
      s << std::setw(8) << "Error" << " from " << func << ": " << buf;
      fWhat = s.str();
    }
    const char* what() const throw () { return fWhat.c_str(); }
  };

  //====================================================================
  struct LockGuard 
  {
	  dcs::fee::Mutex* fMutex;
    LockGuard( dcs::fee::Mutex* l) 
      : fMutex(l)
    {
      fMutex->Lock();
    }
    ~LockGuard() 
    {
      fMutex->UnLock(); 
	}
  };

  //____________________________________________________________________
  // pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;
  dcs::fee::Mutex* mut = dcs::fee::Mutex::Create();
}

//____________________________________________________________________
bool 
dcs::fee::FeeClientLibImp::writeReadData(std::string&  serverName, 
					 size_t&       size, 
					 DataArray&    data, 
					 FlagBits&     flags, 
					 short&        errorCode, 
					 short&        status)
{
  int id  = 0;
  int ret = 0;
  curId   = 0;  // reset id to filter out stucking acknowledge data
  try {
    if(serverName.empty())   
		throw FeeException(__FUNCTION__, "No serverName");
		
    if(size == 0)            
		FeeLogging(logLevelWarning, 1, -1, __FUNCTION__, "Size is zero");
    // This doesn't work, because reads may not have allocated
    // enough memory when we enter here.  However, that memory
    // will be allocated later, so there's no need to worry
    // really.
    if(data.size()<size)     throw FeeException(__FUNCTION__, "Too little data %d < %d",
				      data.size(), size);
    
    size_t osize = size * sizeof(DataArray::value_type);
    id = sendCommand(const_cast<char*>(serverName.c_str()), osize, 
		     reinterpret_cast<char*>(&(data[0])), flags); 
    if(id <= 0) throw FeeException(__FUNCTION__, "id=%id, sendCommand failed", id);
    
    // size        = 0;
    flags       = 0;
    errorCode   = 0;
    status      = 0;
    ret = getAcknowledge(serverName, size, data, id, flags, 
			 errorCode, status);
    
    if(!ret) throw FeeException(__FUNCTION__, "ret=%i, getAcknowledge failed", ret);
  }
  catch (std::exception& e) {
    std::cerr << "FeeClient::writeReadData(" << serverName << "," 
	      << size << "," << &(data[0]) << "," << flags << "," 
	      << errorCode << "," << status << "):\n\t" 
	      << e.what() << std::endl;
    return false;
  }
  return true;
}

//____________________________________________________________________
int
dcs::fee::FeeClientLibImp::getAcknowledge(std::string&  serverName, 
					  size_t&       size, 
					  DataArray&    data, 
					  unsigned int  id, 
					  FlagBits&     flags, 
					  short&        errorCode, 
					  short&        status)
{
  int maxtime      = 50;
  int timeout      = maxtime;
  int usleepPeriod = 50000;
  int done         = false;
  try {
    if(id <= 0)       throw FeeException(__FUNCTION__, "id=%id, invalid id", id);
    
    while(!done && timeout > 0) {
#ifndef WIN32
      usleep(usleepPeriod);
#else
		Sleep(usleepPeriod/1000);
#endif
      timeout--;

      LockGuard l(mut);
      if(id == curId) {
	
	if(curServerName.empty())  throw FeeException(__FUNCTION__, "no curServerName");
	// if(curSize <= 0)           throw FeeException(__FUNCTION__, "no curData");
	
	serverName = curServerName;
	size       = curSize;
	flags      = curFlags;
	errorCode  = curErrorCode;
	status     = curStatus;
	done       = true;
	if (curSize > 0) {
	  if (data.size() < curSize) data.resize(curSize);
	  std::copy(&(curData[0]), &(curData[curSize]), data.begin());
	}
      }
    }
    if(timeout <= 0) 
      throw FeeException(__FUNCTION__, "timeout=%i, no response from FeeServer within timeout", 
	       timeout);
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 0;
  }
  return timeout;
}

//____________________________________________________________________
void
dcs::fee::FeeClientLibImp::FeeLogging(int         type, 
				      int         ref, 
				      int         id, 
				      const char* func, 
				      const char* format, 
				      ...)
{
  if (type > logLevel) return;
  
  static char buf[1024];
  va_list ap;
  va_start(ap, format);
  vsprintf(buf,format, ap);
  va_end(ap);
  std::cerr << "id=" << id << ": "; 
  switch(type){
  case logLevelDebug:
    std::cerr << std::setw(8)  << "Debug"; break;
  case logLevelWarning:
    std::cerr << std::setw(8)  << "Warning"; break;
  case logLevelError: 
    std::cerr << std::setw(8)  << "Error"; break;
  }
  std::cerr << "(" << ref << ") from " << func << ": " << buf << std::endl;
}

//____________________________________________________________________
void
dcs::fee::FeeClientLibImp::acknowledgeHandler(char*        serverName, 
					      unsigned int size, 
					      char*        data, 
					      unsigned int id, 
					      FlagBits     flags, 
					      short        errorCode, 
					      short        status)
{
  try {
    if(!serverName)   throw FeeException(__FUNCTION__, "no serverName");
    if(!*serverName)  throw FeeException(__FUNCTION__, "no *serverName");
    if(!data)         throw FeeException(__FUNCTION__, "no data");
    if(id == 0 && status < 0) {
      std::cerr << "Received status " << status << " from FeeServer\n"
		<< "serverName=" << serverName << "\n"
		<< "size      =" << size << "\n"
		<< "id        =" << id << "\n" 
		<< "flags     =" << flags << "\n"
		<< "errorCode =" << errorCode << std::endl;
    }
    else{
      LockGuard l(mut);
      
      curId        =  id;
      curFlags       = flags;
      curErrorCode  = errorCode;
      curStatus     = status;
      curServerName = serverName;
      curSize       = size / sizeof(DataArray::value_type);
      if (curSize > 0) {
	if (curData.size() < curSize) curData.resize(curSize);
	memcpy(&(curData[0]), data, size);
      }
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}

//____________________________________________________________________
void
dcs::fee::FeeClientLibImp::messageHandler(const dcs::fee::MessageStruct msg)
{
  // cerr << "Received message from " 
  //      << msg.detector << "-detector: " << endl;
  std::cerr << "Message Type: " << msg.eventType << ", " 
	    << "Source: "       << msg.source << ", "
	    << "Date: "         << msg.date << "\n"
	    << "- "             << msg.description << " -\n" 
	    << std::endl;
}

//____________________________________________________________________
//
// EOF
//
